mod test;
use std::collections::HashMap;
use std::env;
use std::fs;
use std::io::{self, Read};

fn main() {
    // read a file's path from the command line, process the file, and print character frequencies
    let args: Vec<String> = env::args().collect();
    if args.len() < 2 {
        eprintln!("Usage: {} <file_path>", args[0]);
        std::process::exit(1);
    }

    let filename = &args[1];
    match calculate_frequency(filename) {
        Ok(freqs) => {
            for (char, count) in freqs.iter() {
                println!("'{}': {}", char, count);
            }
        },
        Err(e) => {
            eprintln!("Error processing file: {}", e);
            std::process::exit(1);
        }
    }
}

fn calculate_frequency(file_path: &str) -> io::Result<HashMap<char, usize>> {
    let mut file = fs::File::open(file_path)?;
    let mut contents = String::new();
    file.read_to_string(&mut contents)?;
    
    let mut freqs = HashMap::new();
    for c in contents.chars() {
        *freqs.entry(c).or_insert(0) += 1;
    }

    Ok(freqs)
}
