#[cfg(test)]
mod tests {
    use super::*;
    use std::collections::HashMap;
    use std::io::Write;
    use tempfile::NamedTempFile;
    use crate::calculate_frequency;

    #[test]
    fn test_calculate_frequency() {
        let mut test_string = "test string";
        let mut expected_freqs = HashMap::new();
        expected_freqs.insert('t', 3);
        expected_freqs.insert('e', 1);
        expected_freqs.insert('s', 2);
        expected_freqs.insert(' ', 1);
        expected_freqs.insert('r', 1);
        expected_freqs.insert('i', 1);
        expected_freqs.insert('n', 1);
        expected_freqs.insert('g', 1);

        let mut temp_file = NamedTempFile::new().unwrap();
        
        write!(temp_file.as_file_mut(), "{}", test_string).unwrap();

        let freqs = calculate_frequency(temp_file.path().to_str().unwrap()).unwrap();
        assert_eq!(freqs, expected_freqs);

        test_string = "Hello World";
        let mut expected_freqs_1 = HashMap::new();
        expected_freqs_1.insert('H', 1);
        expected_freqs_1.insert('e', 1);
        expected_freqs_1.insert('l', 3);
        expected_freqs_1.insert(' ', 1);
        expected_freqs_1.insert('o', 2);
        expected_freqs_1.insert('W', 1);
        expected_freqs_1.insert('r', 1);
        expected_freqs_1.insert('d', 1);

        let mut temp_file = NamedTempFile::new().unwrap();
        // Ensure we can write to the file. You need to call `as_file_mut()` to get a mutable reference to the file.
        write!(temp_file.as_file_mut(), "{}", test_string).unwrap();

        let freqs = calculate_frequency(temp_file.path().to_str().unwrap()).unwrap();
        assert_eq!(freqs, expected_freqs_1);
    }
}
