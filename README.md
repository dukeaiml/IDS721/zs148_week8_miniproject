# Week 8 Mini-Project

## Author
Ziyu Shi

## Requriments
### Implement A Rust Command-Line Tool with Testing
- Rust command-line tool
- Data ingestion/processing
- Unit tests

## Tool functionality
This Rust Command-Line Tool will take a file path as input, then calculate the frequency of each character in the file and prints the results. If the file path is missing, the program will report an error.

## Data processing
This Rust Command-Line Tool will read the content from the input file and convert the content into readable string. Then use the HashMap to calculate the frequency of each character in the input file.

## Testing implementation
Test the Rust Command-Line Tool by the specific code in the `src/test.rs`. Initialize a testing string and the expected result. Run the calculation function with the testing string as the input content, then compare the generated result with the expected result. The **testing report** is [here](./testing_report//test_report.txt).

## Project Steps
### Prepare the development environment
Install Rust and Cargo toolkit on the system with official script.
```
curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh
```
Configure the current shell to activate development environment by:
```
source $HOME/.cargo/env
```
### Create Rust project and implement Command-Line Tool
1. Create a new Rust project:
    ```
    cargo new <PROJECT_NAME>
    ```
2. Add necessary dependencies in `Cargo.toml`.
3. Implement the Command-Line Tool in the `src/main.rs` to read arguments from the terminal command as input. Here is the part of my implementation:
    ```rust
    // read a file's path from the command line, process the file, and print character frequencies
    let args: Vec<String> = env::args().collect();
    if args.len() < 2 {
        eprintln!("Usage: {} <file_path>", args[0]);
        std::process::exit(1);
    }
    ```
### Testing the Command-Line Tool
1. **Generate testing funcation to test the Tool.** I create a file `src/test.rs` to test the main function. Here is the part of my testing implementation:
    ```rust
        let mut test_string = "test string";
        let mut expected_freqs = HashMap::new();
        expected_freqs.insert('t', 3);
        expected_freqs.insert('e', 1);
        expected_freqs.insert('s', 2);
        expected_freqs.insert(' ', 1);
        expected_freqs.insert('r', 1);
        expected_freqs.insert('i', 1);
        expected_freqs.insert('n', 1);
        expected_freqs.insert('g', 1);

        let mut temp_file = NamedTempFile::new().unwrap();
        
        write!(temp_file.as_file_mut(), "{}", test_string).unwrap();

        let freqs = calculate_frequency(temp_file.path().to_str().unwrap()).unwrap();
        assert_eq!(freqs, expected_freqs);
    ```
    Test my Command-Line Tool, please run:
    ```
    cargo test
    ```
    This setup provides a basic framework for a Rust command-line tool that calculates character frequencies in a file and includes unit testing to verify functionality.

    To run the project, please run:
    ```
    cargo run -- <PATH_TO_FILE>
    ```
    Or run:
    ```
    ./character_frequency <PATH_TO_FILE>
    ```
    This will enable the Tool to read the content from the target file and print the result.
    
2. **Generate testing report based on the testing result.** The report can be saved in [test_report.txt](./testing_report/test_report.txt) by run:
    ```
    cargo test 2>&1 | tee ./testing_report/test_report.txt
    ```

## Screenshots
### Testing result by running `cargo test`
![image](./images/cargoTest.png)

### Testing result by running `cargo run -- test.txt`
![image](./images/cargoRun.png)

### Testing report
![image](./images/testReport.png)